```javascript
<template>
	<view class="content">
		<pg-tree :list="list" :selected="selected" :unfoldPath="unfoldPath" :keepLevel="true" :isAllfold="2" :enableChoose="true" :finalChoose="false" :enableEdit="true" @itemClick="itemClick" @itemEdit="itemEdit" @switchFold="switchFold"></pg-tree>
	</view>
</template>
```
#### 当前功能有 
1. 多选，单选，不选、末级选择
2. 保存折叠路径，可以还原折叠状态
3. 编辑、删除item
4. 全部折叠，全部打开
5. 无限分类

##### **props如下：**
##### **list**=>数据格式 [{id:1, name:'蔬菜',isSelected: true, child:[]}]。isSelected不传默认为false
##### **selected**=>选中的项目数组，不传则没有选中的项目。值可以为单个id或者id数组
##### **unfoldPath**=>折叠的路径数组，可以控制折叠状态，可以监听@switchFold获得此值。只有keepLevel为true时有效
##### **keepLevel**=>折叠后打开，是否保存层级信息，默认true
##### **enableChoose**=>是否打开组件内置的点击选中，默认true。 为true时，itemClick中监听到的参数结构为：{item:{},selected:[]}，即返回当前选中的item，和总选中的数组。为false时，只返回item
##### **finalChoose**=>是否只能选中末级的item，默认false。即可以多选,就算为true时，也会返回点击的父级item，可以判断item中的isLast属性是否为true来判断是否为末级
##### **singleSelect**=>是否只能单选，默认false，即可以选择多个item。当enableChoose为true时有效
##### **openSelected**=>初始时是否打开选中的item路径，默认false
##### **isAllfold**=>值的范围为：0,1,2   0就全部折叠，1全部打开，2或其他数则不管（不设置效果一样）

##### **@itemClick**=>监听返回点击的项目信息，和enableChoose有相互作用，参考enableChoose
##### **@itemEdit**=>监听返回编辑的项目信息：{item,isDel:true}或者{item,isEdit:true}
##### **@switchFold**=>每次折叠打开item时返回当前的折叠路径数组

##### ps。样式很简单，rightIcon: 折叠图标，downIcon: 打开图标，selectedIcon: 选中图标。
##### 要加什么功能可以留言
```javascript
<script>
	import pgTree from '@/components/pg-tree/pg-tree';
	export default {
		components:{
			pgTree
		},
		data() {
			return {
				selected: [11111,11112],
				unfoldPath: [1, 11, 111, 1111, 112, 1112, 2],
				list: [
					{
						id: 1,
						name: '肉类',
						child: [
							{
								id: 11,
								name: '生鲜',
								child: [{
									id: 111,
									name: '鱼类',
									child: [{
										id: 1111,
										name: '双目鱼类',
										child: [{
											id: 11111,
											name: '比目鱼',
											isSelected: true
										}]
									}]
								}]
							},
							{
								id: 112,
								name: '哺乳类',
								child: [{
									id: 1112,
									name: '多毛生物',
									child: [{
										id: 11112,
										name: '山羊',
										child: [{
											id: 111112,
											name: '黑山羊'
										}]
									}]
								}]
							},
						]
					},
					{
						id: 2,
						name: '蔬菜',
						child: [
							{
								id: 21,
								name: '白菜',
							},
							{
								id: 22,
								name: '胡萝卜',
							},
							{
								id: 23,
								name: '土豆',
							},
						]
					}
				],
			}
		},
		methods: {
			itemClick(e){
				console.log(e)
				//如果不启用内置的点击选中效果，可以用下面的代码自己赋值selected
				// let id = e.id
				// let isHas = false
				// for(var i = 0; i < this.selected.length; i++){
				// 	if(this.selected[i] == id)
				// 	{
				// 		isHas = true
				// 		this.selected.splice(i,1)
				// 	}
				// }
				// if(!isHas){
				// 	this.selected.push(id)
				// }
				// console.log(this.selected)
			},
			itemEdit(ev){
				console.log(ev)
				if(ev.isEdit){
					uni.showToast({
					    title: '你在编辑:'+ ev.item.name,
					    duration: 2000,
					})
				}
				if(ev.isDel){
					uni.showModal({
						content: '您确定要删除' + ev.item.name +'?',
						showCancel: true,
						complete: e => {
							
						}
					})
				}
			},
			switchFold(e) {
				console.log('当前折叠路径为： ',e)
			}
		}
	}
</script>
```
